__author__='Andrii Kozin'

# sync database
from app.finance import app
import config
from couchdb import Database, Server
from couchdb.design import ViewDefinition
from app.mod_rest import models

server = Server(app.config['COUCHDB_SERVER'])
db = server[app.config['COUCHDB_DATABASE']]

#TODO: extend array of views to generate it during syncing
views = [
    models.Currency.get_all_items, 
    models.BankAccount.get_all_items, 
    models.BankAccount.get_by_currency,
    models.DailyStatus.get_all_items,
    models.DailyStatus.get_by_bank_account,
    models.PaymentPlan.get_all_items,
    models.PaymentPlan.get_by_payment_type,
    models.PaymentType.get_all_items,
    models.Payment.get_all_items,
    models.Payment.get_by_bank_account,
    models.Payment.get_by_payment_type,
    models.Income.get_all_items,
    models.Income.get_by_bank_account,
    models.Income.get_by_income_type,
    models.IncomePlan.get_all_items,
    models.IncomePlan.get_by_income_type,
    models.IncomeType.get_all_items,
    models.User.get_by_internal_token,
    models.User.get_by_oauth_access_token,
    models.User.get_by_external_id
    ]
ViewDefinition.sync_many(db, views)