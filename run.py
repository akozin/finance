__author__ = 'Andrii Kozin'

from app.finance import app
from app import rest_api
from app import route
import config
import argparse

def run_app():
	app.run(host='m.mirsoft.tk', port=8080, debug=True, use_debugger=True, use_reloader=False)

if __name__ == '__main__':
	run_app()
