__author__ = 'Andrii Kozin'

from flask.globals import g
from app.mod_rest import models
from datetime import datetime
from random import randrange
import config
import base64

class AuthenticationController(object):

    def __init__(self):
        self.access_token = None
        self.external_id = None
        self.access_token_update_time = None

    def generate_token(self, access_token, external_id):
        self.access_token = access_token
        self.external_id = external_id
        self.access_token_update_time = datetime.now()
        return randrange(111111, 999999), datetime.now()

    def store_user(self, internal_token, internal_token_update_time):
        _new_user = models.User()
        _new_user.oauth_access_token = self.access_token
        _new_user.oauth_external_id = self.external_id
        _new_user.oauth_access_token_update_time = self.access_token_update_time

        _new_user.internal_toke_update_time = internal_token_update_time

        _new_user = _new_user.store(g.couch)
        _new_user.internal_token = internal_token
        _new_user.store(g.couch)
        return {
                'token' : _new_user.internal_token, 
                'token_update_time' : str(_new_user.internal_toke_update_time),
                'server_pub_key' : base64.b64encode(config.RSA_SERVER_PUB_KEY.save_pkcs1('DER'))
                }

    def remove_user(self, internal_token):
        _kwargs = dict(key=internal_token)
        _user_list = models.User.get_by_internal_token(g.couch, **_kwargs)
        if len(_user_list.rows) != 1:
            return {'error' : 'internal error'}
        _user = _user_list.rows[0]
        if _user is not None:
            _user._data['_deleted'] = True
            _user.store(g.couch)
            return {'id' : _user.id}
        else:
            return {'error' : 'user was not found'}