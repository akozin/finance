__author__='Andrii Kozin'

from flask.ext.restful import abort, Api
from app.finance import app
from mod_rest import controllers

api = Api(app)

##
## Actually setup the Api resource routing here
##

controllers_map = dict(
    bank_account=dict(resource=controllers.BankAccountResourceController, 
        list=controllers.BankAccountsListResourceController),
    currency=dict(resource=controllers.CurrencyResourceController,
        list=controllers.CurrenciesListResourceController),
    daily_status=dict(resource=controllers.DailyStatusResourceController,
        list=controllers.DailyStatusesListResourceController),
    payment=dict(resource=controllers.PaymentResourceController,
        list=controllers.PaymentsListResourceController),
    payment_plan=dict(resource=controllers.PaymentPlanResourceController,
        list=controllers.PaymentPlansListResourceController),
    payment_type=dict(resource=controllers.PaymentTypeResourceController,
        list=controllers.PaymentTypesListResourceController),
    income=dict(resource=controllers.IncomeResourceController,
        list=controllers.IncomesListResourceController),
    income_plan=dict(resource=controllers.IncomePlanResourceController,
        list=controllers.IncomePlansListResourceController),
    income_type=dict(resource=controllers.IncomeTypeResourceController,
        list=controllers.IncomeTypesListResourceController)
)

def generate_rest_v1_api_route(controllers_map):
    for key in controllers_map:
        api.add_resource(
            controllers_map[key]['resource'],
            '/rest/v1/' + key + '/<string:document_id>')
        api.add_resource(
            controllers_map[key]['list'],
            '/rest/v1/' + key + '_list/<int:limit>', # list with limit on return items
            '/rest/v1/' + key + '_list/<string:startkey_docid>', # list starting from _id
            '/rest/v1/' + key + '_list/<int:limit>/<string:startkey_docid>', # pagination
            '/rest/v1/' + key + '_list/<string:req_name>/<string:key>', # any req with key
            '/rest/v1/' + key + '_list/<string:req_name>/<string:key>/<int:limit>', # any req with key and limit on return items
            '/rest/v1/' + key + '_list/<string:req_name>/<string:key>/<string:startkey_docid>', # any req with key and starting from _id
            '/rest/v1/' + key + '_list/<string:req_name>/<string:key>/<int:limit>/<string:startkey_docid>', # prev + limit = pagination
            '/rest/v1/' + key + '_list',
            )

generate_rest_v1_api_route(controllers_map)
