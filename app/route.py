__author__ = 'Andrii Kozin'

from mod_auth.controllers import AuthenticationController
from app.finance import app
import json
from flask.templating import render_template_string, render_template
from mod_auth.views import facebook
import config
from flask import redirect, request, url_for, flash, session

app.secret_key = config.SECRET_KEY

@app.route("/")
def hello():
    # return "Hello World!"
    return render_template('index.html')


@app.route('/test/login/authorized/<access_token>')
def test_aouth_authorized(access_token):
    session['oauth_token'] = (access_token, '')
    me = facebook.get('/me')
    _auth_controller = AuthenticationController()
    _int_id, _update_time = _auth_controller.generate_token(session['oauth_token'], me.data['id'])
    return render_template_string(json.dumps(_auth_controller.store_user(_int_id, _update_time)))


@app.route('/login')
def login():
    return facebook.authorize(callback=url_for('oauth_authorized',
        next=request.args.get('next') or request.referrer or None,
        _external = True))

@app.route('/login/authorized')
@facebook.authorized_handler
def oauth_authorized(resp):
    if resp is None:
        return 'Access denied: reason=%s error=%s' % (
            request.args['error_reason'],
            request.args['error_description']
        )
        
    session['oauth_token'] = (resp['access_token'], '')
    me = facebook.get('/me')
    _auth_controller = AuthenticationController()
    _int_id, _update_time = _auth_controller.generate_token(session['oauth_token'], me.data['id'])
    return render_template_string(json.dumps(_auth_controller.store_user(_int_id, _update_time)))

@app.route('/signup/<access_token>/<external_id>')
def create_user(access_token, external_id):
    _auth_controller = AuthenticationController()
    _int_id, _update_time = _auth_controller.generate_token(access_token, external_id)
    return render_template_string(json.dumps(_auth_controller.store_user(_int_id, _update_time)))

@app.route('/delete/<internal_token>')
def delete_user(internal_token):
    _auth_controller = AuthenticationController()
    return render_template_string(json.dumps(_auth_controller.remove_user(internal_token)))

@facebook.tokengetter
def get_facebook_oauth_token():
    return session.get('oauth_token')