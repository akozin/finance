from flask.globals import g
from common.BaseView import BaseView

__author__ = 'z4i'


class LoginView(BaseView):

    def __init__(self, **kwargs):
        super(LoginView, self).__init__(page='settings/login.html')
        self._load_data(**kwargs)

    def _load_data(self, **kwargs):
        names = ['Andrii']

        '''for _id in g.couch:
            doc = g.couch[_id]

            names.append(doc['Name'])
        '''
        self.add_kwarg('name', names)
