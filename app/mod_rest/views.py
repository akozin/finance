__author__='Andrii Kozin'

def generate_func_body(doc_type):
    return '\
    function(doc) {\
        if (doc.user_id && doc.doc_type == \'' + doc_type +'\'){ \
            emit([1, doc.user_id, doc._id], null); \
            emit([2, doc.user_id], null); }\
    }'

def generate_func_body_by_type_and_key(doc_type, key):
    return '\
    function(doc){\
        if (doc.user_id && doc.doc_type == \''+ doc_type +'\'){\
            emit([1, doc.user_id, doc.'+key+'], null)\
        }\
    }'

def generate_func_body_by_type_and_key_(doc_type, key):
    return '\
    function(doc){\
        if (doc.doc_type == \''+ doc_type +'\'){\
            emit(doc.'+key+', null)\
        }\
    }'


all_currency_types_func = generate_func_body('currency_doc_type')
all_bank_accounts_func = generate_func_body('bank_account_doc_type')
all_daily_statuses_func = generate_func_body('daily_status_doc_type')
all_incomes_func = generate_func_body('income_doc_type')
all_income_types_func = generate_func_body('income_type_doc_type')
all_income_plans_func = generate_func_body('income_plan_doc_type')
all_payments_func = generate_func_body('payment_doc_type')
all_payment_types_func = generate_func_body('payment_type_doc_type')
all_payment_plans_func = generate_func_body('payment_plan_doc_type')

get_bank_account_by_currency_func = generate_func_body_by_type_and_key('bank_account_doc_type', 'currency')
daily_statuses_by_bank_account_func = generate_func_body_by_type_and_key('daily_status_doc_type','bank_account')
incomes_by_bank_account_func = generate_func_body_by_type_and_key('income_doc_type', 'bank_account')
incomes_by_income_type_func = generate_func_body_by_type_and_key('income_doc_type', 'income_type')
income_plans_by_income_type_func = generate_func_body_by_type_and_key('income_plan_doc_type', 'income_type')
payments_by_bank_account_func = generate_func_body_by_type_and_key('payment_doc_type', 'bank_account')
payments_by_payment_type_func = generate_func_body_by_type_and_key('payment_doc_type', 'payment_type')
payment_plans_by_payment_type_func = generate_func_body_by_type_and_key('payment_plan_doc_type', 'payment_type')

user_by_oauth_token_func = generate_func_body_by_type_and_key_('user_doc_type', 'oauth_access_token')
user_by_internal_token_func = generate_func_body_by_type_and_key_('user_doc_type', 'internal_token')
user_by_external_id_func = generate_func_body_by_type_and_key_('user_doc_type', 'oauth_external_id')

