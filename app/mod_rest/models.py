__author__='Andrii Kozin'

from flask.globals import g
from flaskext.couchdb import mapping, Document, DecimalField, TextField, DateTimeField, ListField, BooleanField, ViewField
import views
import datetime

class BaseDocument(Document):
    user_id = TextField()

class Currency(BaseDocument):
    doc_type = 'currency_doc_type'

    name = TextField()
    short_symbol = TextField()

    get_all_items = mapping.ViewField('currency', views.all_currency_types_func, include_docs=True)

class BankAccount(BaseDocument):
    doc_type = 'bank_account_doc_type'

    name = TextField()
    currency = TextField()
    opened = DateTimeField(default=datetime.datetime.now)

    get_all_items = mapping.ViewField('bank_account', views.all_bank_accounts_func, include_docs=True)
    get_by_currency = mapping.ViewField('bank_account', views.get_bank_account_by_currency_func, include_docs=True)

class DailyStatus(BaseDocument):
    doc_type = 'daily_status_doc_type'
    
    balance = DecimalField()
    date = DateTimeField(default=datetime.datetime.now)
    bank_account = TextField()

    get_all_items = mapping.ViewField('daily_status', views.all_daily_statuses_func, include_docs=True)
    get_by_bank_account = mapping.ViewField('daily_status', views.daily_statuses_by_bank_account_func, include_docs=True)

class Income(BaseDocument):
    doc_type = 'income_doc_type'

    amount = DecimalField()
    date = DateTimeField(default=datetime.datetime.now)
    bank_account = TextField()
    income_type = TextField()

    get_all_items = mapping.ViewField('income', views.all_incomes_func, include_docs=True)
    get_by_bank_account = mapping.ViewField('income', views.incomes_by_bank_account_func, include_docs=True)
    get_by_income_type = mapping.ViewField('income', views.incomes_by_income_type_func, include_docs=True)

class IncomeType(BaseDocument):
    doc_type = 'income_type_doc_type'

    name = TextField()

    get_all_items = mapping.ViewField('income_type', views.all_income_types_func, include_docs=True)

class IncomePlan(BaseDocument):
    doc_type = 'income_plan_doc_type'

    amount = DecimalField()
    income_type = TextField()
    from_date = DateTimeField(default=datetime.datetime.now)
    to_date = DateTimeField(default=(datetime.datetime.now()+datetime.timedelta(7))) 

    get_all_items = mapping.ViewField('income_plan', views.all_income_plans_func, include_docs=True)
    get_by_income_type = mapping.ViewField('income_plan', views.income_plans_by_income_type_func, include_docs=True)

class Payment(BaseDocument):
    doc_type = 'payment_doc_type'

    amount = DecimalField()
    date = DateTimeField(default=datetime.datetime.now)
    bank_account = TextField()
    payment_type = TextField()

    get_all_items = mapping.ViewField('payment', views.all_payments_func, include_docs=True)
    get_by_bank_account = mapping.ViewField('payment', views.payments_by_bank_account_func, include_docs=True)
    get_by_payment_type = mapping.ViewField('payment', views.payments_by_payment_type_func, include_docs=True)

class PaymentType(BaseDocument):
    doc_type = 'payment_type_doc_type'

    name = TextField()

    get_all_items = mapping.ViewField('payment_type', views.all_payment_types_func, include_docs=True)

class PaymentPlan(BaseDocument):
    doc_type = 'payment_plan_doc_type'

    amount = DecimalField()
    payment_type = TextField()
    from_date = DateTimeField(default=datetime.datetime.now)
    to_date = DateTimeField(default=(datetime.datetime.now() + datetime.timedelta(7)))

    get_all_items = mapping.ViewField('payment_plan', views.all_payment_plans_func, include_docs=True)
    get_by_payment_type = mapping.ViewField('payment_plan', views.payment_plans_by_payment_type_func, include_docs=True)

class User(Document):
    doc_type = 'user_doc_type'

    oauth_access_token = TextField()
    oauth_external_id = TextField()
    oauth_provider = TextField(default='facebook')
    oauth_access_token_update_time = DateTimeField(default=datetime.datetime.now)

    internal_token = TextField()
    internal_toke_update_time = DateTimeField(default=datetime.datetime.now)

    get_by_oauth_access_token = mapping.ViewField('user', views.user_by_oauth_token_func, include_docs=True)
    get_by_internal_token = mapping.ViewField('user', views.user_by_internal_token_func, include_docs=True)
    get_by_external_id = mapping.ViewField('user', views.user_by_external_id_func, include_docs=True)
