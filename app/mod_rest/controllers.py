__author__='Andrii Kozin'

from app.mod_rest import models
from flask import request
from common.resource_controller import ResourceController, ListResourceController, ParametersController
from flask.ext.restful import reqparse

class ParametersControllerImpl(ParametersController):
    def __init__(self, *args, **kwargs):
        super(ParametersControllerImpl, self).__init__(*args, **kwargs)
        _parser = reqparse.RequestParser()
        _parser.add_argument('item', type=dict)
        _parser.add_argument('items', type=dict)
        _parser.add_argument('token', type=str)
        self.args = _parser.parse_args()

    def get_argument_by_name(self, item_name):
        return self.args[item_name]

    def get_item(self):
        return self.get_argument_by_name('item')

    def get_items(self):
        return self.get_argument_by_name('items')

    def get_token(self):
        return self.get_argument_by_name('token')

class BankAccountResourceController(ResourceController, ParametersControllerImpl):
    def __init__(self, *args, **kwargs):
        super(BankAccountResourceController, self).__init__(*args, **kwargs)
        self.resource_type = models.BankAccount

class BankAccountsListResourceController(ListResourceController, ParametersControllerImpl):
    def __init__(self):
        super(BankAccountsListResourceController, self).__init__()
        self.resource_type = models.BankAccount

class CurrencyResourceController(ResourceController, ParametersControllerImpl):
    def __init__(self):
        super(CurrencyResourceController, self).__init__()
        self.resource_type = models.Currency

class CurrenciesListResourceController(ListResourceController, ParametersControllerImpl):
    def __init__(self):
        super(CurrenciesListResourceController, self).__init__()
        self.resource_type =  models.Currency

class DailyStatusResourceController(ResourceController, ParametersControllerImpl):
    def __init__(self):
        super(DailyStatusResourceController, self).__init__()
        self.resource_type =  models.DailyStatus

class DailyStatusesListResourceController(ListResourceController, ParametersControllerImpl):
    def __init__(self):
        super(DailyStatusesListResourceController, self).__init__()
        self.resource_type =  models.DailyStatus

class PaymentPlanResourceController(ResourceController, ParametersControllerImpl):
    def __init__(self):
        super(PaymentPlanResourceController, self).__init__()
        self.resource_type =  models.PaymentPlan

class PaymentPlansListResourceController(ListResourceController, ParametersControllerImpl):
    def __init__(self):
        super(PaymentPlansListResourceController, self).__init__()
        self.resource_type =  models.PaymentPlan

class PaymentTypeResourceController(ResourceController, ParametersControllerImpl):
    def __init__(self):
        super(PaymentTypeResourceController, self).__init__()
        self.resource_type = models.PaymentType

class PaymentTypesListResourceController(ListResourceController, ParametersControllerImpl):
    def __init__(self):
        super(PaymentTypesListResourceController, self).__init__()
        self.resource_type =  models.PaymentType

class PaymentResourceController(ResourceController, ParametersControllerImpl):
    def __init__(self):
        super(PaymentResourceController, self).__init__()
        self.resource_type = models.Payment

class PaymentsListResourceController(ListResourceController, ParametersControllerImpl):
    def __init__(self):
        super(PaymentsListResourceController, self).__init__()
        self.resource_type = models.Payment

class IncomeResourceController(ResourceController, ParametersControllerImpl):
    def __init__(self):
        super(IncomeResourceController, self).__init__()
        self.resource_type = models.Income

class IncomesListResourceController(ListResourceController, ParametersControllerImpl):
    def __init__(self):
        super(IncomesListResourceController, self).__init__()
        self.resource_type = models.Income

class IncomePlanResourceController(ResourceController, ParametersControllerImpl):
    def __init__(self):
        super(IncomePlanResourceController, self).__init__()
        self.resource_type = models.IncomePlan

class IncomePlansListResourceController(ListResourceController, ParametersControllerImpl):
    def __init__(self):
        super(IncomePlansListResourceController, self).__init__()
        self.resource_type = models.IncomePlan

class IncomeTypeResourceController(ResourceController, ParametersControllerImpl):
    def __init__(self):
        super(IncomeTypeResourceController, self).__init__()
        self.resource_type = models.IncomeType

class IncomeTypesListResourceController(ListResourceController, ParametersControllerImpl):
    def __init__(self):
        super(IncomeTypesListResourceController, self).__init__()
        self.resource_type = models.IncomeType
