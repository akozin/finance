
financeApp.controller("mainCtrl", ['$scope', '$log', 'authService', function($scope, $log, authService){
    $log.info('in mainCtrl ' + authService.loggedIn);

    $scope.fbInfo = {
        loggedIn : authService.loggedIn,
        fbName : authService.fbName
    };
    
    $scope.observer = function(){
        $scope.$apply(function(){
            $scope.fbInfo.loggedIn = authService.loggedIn;
            $scope.fbInfo.fbName = authService.fbName;
            $log.warn('observer was called!' + $scope.fbInfo.fbName + ' ' + $scope.fbInfo.loggedIn + '|' + authService.loggedIn);
        });
    };

    authService.addObserver($scope.observer);
}]);