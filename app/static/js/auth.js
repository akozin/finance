
financeApp.factory("authService", ['$log', function($log){

    var sInstance = {
        loggedIn : false,
        fbName : 'mmmmm', 
        
        // This is called with the results from from FB.getLoginStatus().
        statusChangeCallback : function(response) {
            $log.info('statusChangeCallback');
            // The response object is returned with a status field that lets the
            // app know the current login status of the person.
            // Full docs on the response object can be found in the documentation
            // for FB.getLoginStatus().
            if (response.status === 'connected') {
              // Logged into your app and Facebook.
              sInstance.testAPI();
            } 
        },

        // This function is called when someone finishes with the Login
        // Button.  See the onlogin handler attached to it in the sample
        // code below.
        checkLoginState : function() {
            $log.info('login for: ' + sInstance.fbName + '; loggedIn: ' + sInstance.loggedIn);
            FB.getLoginStatus(function(response) {
              sInstance.statusChangeCallback(response);
            });
        },

        // Here we run a very simple test of the Graph API after login is
        // successful.  See statusChangeCallback() for when this call is made.
        testAPI : function() {
            $log.info('Welcome!  Fetching your information.... ');
            FB.api('/me', function(response) {
              sInstance.loggedIn = true;
              sInstance.fbName = response.name;
              $log.info('Successful login for: ' + sInstance.fbName + '; loggedIn: ' + sInstance.loggedIn);

              for (var i = 0; i < sInstance.observers.length; i++) {
                  sInstance.observers[i]();
              };
            });
        },

        addObserver : function(callBack){
            sInstance.observers.push(callBack);
        },

        observers : []
    };
    
    $log.info('in authController');

    return sInstance;
}]);