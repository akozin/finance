#!/bin/sh

set -e
set -x

RUN_TESTS="env/bin/python test_suite.py > logs/tests_run_$today.log 2>&1 & "

for i in {0..5}
do
    sleep 1.5
    #python -m unittest tests.testcases.test_bank_account.TestBankAccount.test_get_bank_account_with_limit > logs/tests_bank_account_$i.log 2>&1 
    python test_suite.py > logs/tests_suite_run_$i.log 2>&1 & 
done

# today=`date +%Y-%m-%d.%H:%M:%S`
# mkdir -p logs
# env/bin/python run.py > logs/tests_run_$today.log 2>&1 & 
# SERVER_PID=${!} 

# trap "kill ${SERVER_PID}" 0

# echo "wait until server starting..."
# sleep 3


# set +e

# ${RUN_TESTS}
# EXIT_CODE=$?


# kill -s HUP ${SERVER_PID}
# wait ${SERVER_PID}
# trap - 0

# exit ${EXIT_CODE}