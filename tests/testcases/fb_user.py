__author__='Andrii Kozin'
import config
import facebook_api

class FbUser(object):

    facebook_token = None
    facebook_user = None
    test_cases = 0

    @staticmethod
    def get_facebook_token():
        if FbUser.facebook_token is None:
            FbUser.facebook_token = facebook_api.app_token(config.FACEBOOK_APP_ID, config.FACEBOOK_APP_SECRET)
        return FbUser.facebook_token

    @staticmethod
    def get_facebook_user(test_cases = 1):
        if FbUser.facebook_user is None:
            FbUser.facebook_user = facebook_api.create(config.FACEBOOK_APP_ID, FbUser.get_facebook_token())
            print "--------------------------------------"
            print "Created FbUser with id: ", FbUser.facebook_user['id']
            print "--------------------------------------"
            FbUser.test_cases = test_cases
        return FbUser.facebook_user

    @staticmethod
    def remove_facebook_user():
        FbUser.test_cases -= 1
        if FbUser.test_cases > 0:
            print "--------------------------------------"
            print "Skip to remove user ", FbUser.test_cases
            print "--------------------------------------"
            return 
        elif FbUser.test_cases == 0:
            print "--------------------------------------"
            print "Removed FbUser with id: ", FbUser.get_facebook_user()['id']
            facebook_api.delete(FbUser.get_facebook_user()['id'], FbUser.get_facebook_token())
            print "--------------------------------------"
        elif FbUser.test_cases < 0:
            print "--------------------------------------"
            print "Nothing to remove"
            print "--------------------------------------"
            
        

    