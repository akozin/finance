__author__ = 'Vlad'
import csv
from file_name_generator import file_time

class average_time:

    @staticmethod
    def get_array_with_time(name_csv_file):
        array_time = []
        with open(name_csv_file, 'rb') as f:
            file_reader = csv.reader(f, delimiter=',')
            for row in file_reader:
                array_time.append(row)
        return array_time

    @staticmethod
    def get_array_with_average_time(array_of_file_names):
        dict_of_times = dict()
        for file_name in array_of_file_names:
            array_of_time = average_time.get_array_with_time(file_name)
            for item in array_of_time:
                if item[0] in dict_of_times:
                    dict_of_times[item[0]].append(float(item[1]))
                else:
                    dict_of_times[item[0]] = [float(item[1])]

        array_of_array_of_average_min_max_time = []
        for i in dict_of_times:
            array_of_average_min_max_time = []
            array_of_average_min_max_time.append(i)
            array_of_average_min_max_time.append(sum(dict_of_times[i])/len(dict_of_times[i]))
            array_of_average_min_max_time.append(min(dict_of_times[i]))
            array_of_average_min_max_time.append(max(dict_of_times[i]))
            array_of_array_of_average_min_max_time.append(array_of_average_min_max_time)

        return array_of_array_of_average_min_max_time


    @staticmethod
    def create_csv_file_with_average_time(file_name, array_of_average_time):
        for i in array_of_average_time:
            with open(file_name, 'a+b') as f:
                    writer = csv.writer(f, delimiter=",")
                    writer.writerow(i)

if __name__ == '__main__':
    file_name = "average_time_" + file_time.generate_file_name()
    array_of_file_names = ['2015-01-29_23-35-13.100124.csv',
                           '2015-01-29_23-35-14.437530.csv',
                           '2015-01-29_23-35-17.577790.csv',
                           '2015-01-29_23-35-18.794566.csv',
                           '2015-01-29_23-35-20.822455.csv',
                           '2015-01-29_23-35-23.996819.csv']
    array_of_average_time = average_time.get_array_with_average_time(array_of_file_names)
    average_time.create_csv_file_with_average_time(file_name, array_of_average_time)