__author__='Andrii Kozin'

import facebook_api
from unittest import TestCase
import requests
import config

class TestFacebookUsers(TestCase):
    
    FINANCE_SERVER_URL = 'http://m.mirsoft.tk:8080'

    @classmethod
    def setUpClass(cls):
        cls.access_token = facebook_api.app_token(config.FACEBOOK_APP_ID, config.FACEBOOK_APP_SECRET)
        cls.users = []
        print cls.access_token, cls.users
        cls.users.append(facebook_api.create(config.FACEBOOK_APP_ID, cls.access_token))

    @classmethod
    def tearDownClass(cls):
        print [user['id'] for user in cls.users]
        for user in cls.users:
            facebook_api.delete(user['id'], cls.access_token)
            print "User %s removed" % user['id']

    def test_login_to_the_system(self):
        _user = self.users[0]
        _url_to_login = self.FINANCE_SERVER_URL + '/test/login/authorized/' + _user['access_token']
        resp = requests.get(_url_to_login)
        print resp.json()

    def test_print_all_test_users(self):
        _users = facebook_api.get(config.FACEBOOK_APP_ID, self.access_token)

        for _user in _users:
            print _user['id']
