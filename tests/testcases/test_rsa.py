__author__='Andrii Kozin'

from unittest import TestCase
import rsa
import base64
import config
import datetime

class TestRSA(TestCase):

    def test_generate_keys(self):
        server_pubkey = config.RSA_SERVER_PUB_KEY
        server_privkey = config.RSA_SERVER_PRIV_KEY

        (client_pubkey, client_privkey) = rsa.newkeys(512)

        
        _current_time = str(datetime.datetime.now())
        _message_to_encrypt = u"2223344".encode('ascii') + '|' + _current_time
        encrypted_message = rsa.encrypt(_message_to_encrypt, client_pubkey)
        
        dectypted_message = rsa.decrypt(encrypted_message, client_privkey)

        self.assertEqual(_message_to_encrypt, dectypted_message)

    def test_encode_decode_keys(self):
        server_pubkey = config.RSA_SERVER_PUB_KEY

        der_encoded = base64.b64encode(server_pubkey.save_pkcs1('DER'))

        raw_key = rsa.PublicKey.load_pkcs1(base64.b64decode(der_encoded), 'DER')

        self.assertEqual(server_pubkey, raw_key)