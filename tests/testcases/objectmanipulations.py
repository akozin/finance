__author__='Andrii Kozin'

import requests
from random import randrange
import rsa
import base64
import datetime
from fb_user import FbUser

class ObjectManipulations:
    headers = {'Content-Type': 'application/json'}
    facebook_token = None
    fb_user_id = None
    fb_access_token = None
    server_address = "http://m.mirsoft.tk:8080/"

    @staticmethod
    def generate_user():
        # 1. authorize user at our server
        ObjectManipulations.facebook_token = FbUser.get_facebook_token()
        ObjectManipulations.fb_user_id = FbUser.get_facebook_user()['id']
        ObjectManipulations.fb_access_token = FbUser.get_facebook_user()['access_token']
        address = ObjectManipulations.server_address + "test/login/authorized/" + ObjectManipulations.fb_access_token
        r = requests.get(address, headers=ObjectManipulations.headers)
        r_json = r.json()
        _facebook_user_id = ObjectManipulations.fb_user_id
        _internal_token = r_json['token']
        _token_update_time = r_json['token_update_time']
        _server_pub_key = rsa.PublicKey.load_pkcs1(base64.b64decode(r_json['server_pub_key']), 'DER')
        return (_facebook_user_id, _internal_token, _token_update_time, _server_pub_key)

    @staticmethod
    def remove_user(internal_token, facebook_user_id):
        # 1. remove user from our server
        address = ObjectManipulations.server_address + "delete/" + internal_token
        r = requests.get(address, headers=ObjectManipulations.headers)

        # 2. remove fb user
        FbUser.remove_facebook_user()
        
    @staticmethod
    def sign_token_with_server_pub_key(token_to_sign, server_pub_key):
        _current_time = str(datetime.datetime.now())
        _message_to_encrypt = token_to_sign.encode('ascii') + '|' + _current_time
        return base64.b64encode(rsa.encrypt(_message_to_encrypt, server_pub_key))

    @staticmethod
    def create_new_item(token, server_pub_key, payload, key):
        payload['token'] = ObjectManipulations.sign_token_with_server_pub_key(token, server_pub_key)
        address = ObjectManipulations.server_address + "rest/v1/" + key
        r = requests.post(address, json=payload, headers=ObjectManipulations.headers)
        return r

    @staticmethod
    def delete_item(token, server_pub_key, key, item_ids):
        address = ObjectManipulations.server_address + "rest/v1/" + key 
        _items_to_delete = {} #{'items': {'0': {'name': 'test1'}}}
        for _item_index in xrange(len(item_ids)):
            _items_to_delete[str(_item_index)] = item_ids[_item_index].encode('ascii')
        payload = dict(token=ObjectManipulations.sign_token_with_server_pub_key(token, server_pub_key))
        payload['items'] = _items_to_delete
        r = requests.delete(address, json=payload)
        return r

    @staticmethod
    def get_item(token, server_pub_key, key, item_id = ""):
        address = ObjectManipulations.server_address + "rest/v1/" + key + item_id
        payload = dict(token=ObjectManipulations.sign_token_with_server_pub_key(token, server_pub_key))
        r = requests.get(address, data=payload)
        return r

    @staticmethod
    def get_item_with_limit(token, server_pub_key, key, limit, separator = "", startkey_docid = ""):
        address = ObjectManipulations.server_address + "rest/v1/" + key + limit + separator + startkey_docid
        payload = dict(token=ObjectManipulations.sign_token_with_server_pub_key(token, server_pub_key))
        r = requests.get(address, data=payload)
        return r

    @staticmethod
    def get_array_ids_doc(resp_data):
        i = 0
        array_ids_doc = []
        while i < (len(resp_data)-1):
            array_ids_doc.append(resp_data[i]['_id'])
            i += 1
        return array_ids_doc

    @staticmethod
    def id_not_in_array(resp_data, array_ids_doc):
        i = 0
        flag = True
        while i < (len(resp_data)-1):
            if resp_data[i]['_id'] in array_ids_doc:
                flag = False
            i += 1
        return flag

    @staticmethod
    def put_item(token, server_pub_key, payload, key, item_id):
        address = ObjectManipulations.server_address + "rest/v1/" + key + item_id
        payload['token'] = ObjectManipulations.sign_token_with_server_pub_key(token, server_pub_key)
        r = requests.put(address, json=payload, headers=ObjectManipulations.headers)
        return r

