__author__ = 'Vlad'
from datetime import datetime, timedelta, date, time as dt_time

class file_time:
    file_name = None

    @staticmethod
    def generate_file_name():
        if file_time.file_name is None:
            file_time.file_name = str(datetime.now()).replace(' ', '_').replace(':', '-')
            file_time.file_name = file_time.file_name + ".csv"
            print file_time.file_name
        return file_time.file_name