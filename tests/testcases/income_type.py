__author__='Andrii Kozin'

import unittest
import requests
from helper import repeat, measure_time
from objectmanipulations import ObjectManipulations

class TestIncomeType(unittest.TestCase):
    payload = {'items': {'0': {'name': 'test1'}}}
    #payload = dict(items=list([dict(name='test1')]))
    payload_for_put = dict(item=dict(name='test2'))
    N = 10
    limit = 3

    @classmethod
    def setUpClass(self):
        self.ids_to_delete = []
        (self._facebook_user_id, self._internal_token, self._token_update_time, self._server_pub_key) = ObjectManipulations.generate_user()

    @classmethod
    def tearDownClass(self):
        ObjectManipulations.delete_item(self._internal_token, self._server_pub_key, "income_type_list", self.ids_to_delete)
        ObjectManipulations.remove_user(self._internal_token, self._facebook_user_id)

    @measure_time(10)
    @repeat(10)
    def test_create_income_type(self):
        r = ObjectManipulations.create_new_item(self._internal_token, self._server_pub_key, self.payload, "income_type_list")
        self.assertEqual(r.status_code, 201)
        resp_data = r.json()
        self.ids_to_delete.append(resp_data['0']['_id'])
        self.assertEqual(resp_data['0']['doc_type'], 'income_type_doc_type')

    @measure_time()
    def test_delete_income_type_after_create_one(self):
        resp_data = ObjectManipulations.create_new_item(self._internal_token, self._server_pub_key, self.payload, "income_type_list").json()
        id_to_delete = resp_data['0']['_id']
        r = ObjectManipulations.delete_item(self._internal_token, self._server_pub_key, "income_type_list", [id_to_delete])
        self.assertEqual(r.status_code, 200)
        resp_data = r.json()
        self.assertEqual(resp_data['0']['_id'], id_to_delete)

    @measure_time()
    def test_get_income_types(self):
        r = ObjectManipulations.get_item(self._internal_token, self._server_pub_key, "income_type_list")
        resp_data = r.json()
        if len(resp_data):
            for _item in resp_data:
                self.assertTrue(_item['_id'] in self.ids_to_delete)
        self.assertEqual(r.status_code, 200)

    @measure_time()
    def test_get_income_type(self):
       resp_data = ObjectManipulations.create_new_item(self._internal_token, self._server_pub_key, self.payload, "income_type_list").json()
       self.ids_to_delete.append(resp_data['0']['_id'])
       id_to_get = resp_data['0']['_id']
       r = ObjectManipulations.get_item(self._internal_token, self._server_pub_key, "income_type/", id_to_get)
       self.assertEqual(r.status_code, 200)
       resp_data = r.json()
       self.assertEqual(resp_data['_id'], id_to_get)

    @measure_time()
    def test_put_income_type(self):
       resp_data = ObjectManipulations.create_new_item(self._internal_token, self._server_pub_key, self.payload, "income_type_list").json()
       id_to_put = resp_data['0']['_id']
       self.ids_to_delete.append(id_to_put)
       r = ObjectManipulations.put_item(self._internal_token, self._server_pub_key, self.payload_for_put, "income_type/", id_to_put)
       resp_data = r.json()
       self.assertEqual(r.status_code, 200)
       self.assertEqual(id_to_put,resp_data['_id'])

    @measure_time()
    def test_get_income_type_with_limit(self):
        i = 0
        _payload_items = {}
        while i < self.N:
            _payload_items[str(i)] = {'name': 'test1'}
            i += 1
        _payload = {'items' : _payload_items}
        resp_data = ObjectManipulations.create_new_item(self._internal_token, self._server_pub_key, _payload, "income_type_list").json()

        for i in resp_data:
            self.ids_to_delete.append(resp_data[i]['_id'])

        r = ObjectManipulations.get_item_with_limit(self._internal_token, self._server_pub_key, "income_type_list/", str(self.limit))
        self.assertEqual(r.status_code, 200)
        resp_data = r.json()
        array_ids_doc = ObjectManipulations.get_array_ids_doc(resp_data)
        while (len(resp_data) <= self.limit):
            r = ObjectManipulations.get_item_with_limit(self._internal_token, self._server_pub_key, "income_type_list/", str(self.limit), '/', resp_data[len(resp_data)-1]['_id'])
            self.assertEqual(r.status_code, 200)
            resp_data = r.json()
            self.assertEqual(ObjectManipulations.id_not_in_array(resp_data, array_ids_doc), True)
            array_ids_doc.extend(ObjectManipulations.get_array_ids_doc(resp_data))
            if (len(resp_data)==1):
                break