__author__='Andrii Kozin'

import unittest
import requests
from helper import repeat, measure_time
from objectmanipulations import ObjectManipulations

class TestBankAccount(unittest.TestCase):
    payload = {'items': {'0': {'currency': '1', 'name': 'test1'}}}
    #payload = dict(item=dict(currency='1', name='test1'))
    payload_for_put = dict(item=dict(currency='1', name='test2'))
    CURRENCY_ELEMENT_NUMBER = 3
    BANK_ACCOUNT_ELEMENT_NUMBER = 3
    N = 10
    limit = 3

    @classmethod
    def setUpClass(self):
        self.ids_to_delete = []
        self.ids_to_delete_for_currency = []
        (self._facebook_user_id, self._internal_token, self._token_update_time, self._server_pub_key) = ObjectManipulations.generate_user()

    @classmethod
    def tearDownClass(self):
        ObjectManipulations.delete_item(self._internal_token, self._server_pub_key, "bank_account_list", self.ids_to_delete)
        ObjectManipulations.delete_item(self._internal_token, self._server_pub_key, "currency_list", self.ids_to_delete_for_currency)
        ObjectManipulations.remove_user(self._internal_token, self._facebook_user_id)

    @measure_time(10)
    @repeat(10)
    def test_create_bank_account(self):
        r = ObjectManipulations.create_new_item(self._internal_token, self._server_pub_key, self.payload, "bank_account_list")
        self.assertEqual(r.status_code, 201)
        resp_data = r.json()
        self.ids_to_delete.append(resp_data['0']['_id'])
        self.assertEqual(resp_data['0']['currency'], '1')
        self.assertEqual(resp_data['0']['doc_type'], 'bank_account_doc_type')

    @measure_time()
    def test_delete_bank_account_after_create_one(self):
        resp_data = ObjectManipulations.create_new_item(self._internal_token, self._server_pub_key, self.payload, "bank_account_list").json()
        id_to_delete = resp_data['0']['_id']
        r = ObjectManipulations.delete_item(self._internal_token, self._server_pub_key, "bank_account_list", [id_to_delete])
        self.assertEqual(r.status_code, 200)
        resp_data = r.json()
        self.assertEqual(resp_data['0']['_id'], id_to_delete)

    @measure_time()
    def test_get_bank_accounts(self):
        r = ObjectManipulations.get_item(self._internal_token, self._server_pub_key, "bank_account_list")
        resp_data = r.json()
        if len(resp_data):
            for _item in resp_data:
                self.assertTrue(_item['_id'] in self.ids_to_delete)
        self.assertEqual(r.status_code, 200)

    @measure_time()
    def test_get_bank_account(self):
       resp_data = ObjectManipulations.create_new_item(self._internal_token, self._server_pub_key, self.payload, "bank_account_list").json()
       self.ids_to_delete.append(resp_data['0']['_id'])
       id_to_get = resp_data['0']['_id']
       r = ObjectManipulations.get_item(self._internal_token, self._server_pub_key, "bank_account/", id_to_get)
       self.assertEqual(r.status_code, 200)
       resp_data = r.json()
       self.assertEqual(resp_data['_id'], id_to_get)

    @measure_time()
    def test_put_bank_account(self):
       resp_data = ObjectManipulations.create_new_item(self._internal_token, self._server_pub_key, self.payload, "bank_account_list").json()
       id_to_put = resp_data['0']['_id']
       self.ids_to_delete.append(id_to_put)
       r = ObjectManipulations.put_item(self._internal_token, self._server_pub_key, self.payload_for_put, "bank_account/", id_to_put)
       resp_data = r.json()
       self.assertEqual(r.status_code, 200)
       self.assertEqual(id_to_put, resp_data['_id'])

    @measure_time()
    def test_get_by_currency(self):
        i = 0
        _payload_items = {}
        while i < self.CURRENCY_ELEMENT_NUMBER:
            _payload_items[str(i)] = dict(name='Euro', short_symbol='E')
            i += 1
        _payload = {'items' : _payload_items}
        resp_data = ObjectManipulations.create_new_item(self._internal_token, self._server_pub_key, _payload, "currency_list").json()

        for item_index in resp_data:
            currency_item = resp_data[item_index]
            self.ids_to_delete_for_currency.append(currency_item['_id'])
            i = 0
            _payload_items = {}
            while i < self.BANK_ACCOUNT_ELEMENT_NUMBER:
                _payload_items[str(i)] = dict(currency=currency_item['_id'], name='test1')
                i += 1
            _payload = {'items' : _payload_items}
            resp_data1 = ObjectManipulations.create_new_item(self._internal_token, self._server_pub_key, _payload, "bank_account_list").json()
            for item in resp_data1:
                bank_account_item = resp_data1[item]
                self.ids_to_delete.append(bank_account_item['_id'])
            r = ObjectManipulations.get_item(self._internal_token, self._server_pub_key, "bank_account_list/get_by_currency/", currency_item['_id'])
            resp_data2 = r.json()
            self.assertEqual(len(resp_data2), self.BANK_ACCOUNT_ELEMENT_NUMBER)
            self.assertEqual(r.status_code, 200)
            for _item in resp_data2:
                self.assertTrue(_item['currency'] == currency_item['_id'])

    @measure_time()
    def test_get_bank_account_with_limit(self):
        i = 0
        _payload_items = {}
        while i < self.N:
            _payload_items[str(i)] = {'currency': '1', 'name': 'test1'}
            i += 1
        _payload = {'items' : _payload_items}
        resp_data = ObjectManipulations.create_new_item(self._internal_token, self._server_pub_key, _payload, "bank_account_list").json()

        for i in resp_data:
            self.ids_to_delete.append(resp_data[i]['_id'])

        r = ObjectManipulations.get_item_with_limit(self._internal_token, self._server_pub_key, "bank_account_list/", str(self.limit))
        self.assertEqual(r.status_code, 200)
        resp_data = r.json()
        array_ids_doc = ObjectManipulations.get_array_ids_doc(resp_data)
        while (len(resp_data) <= self.limit):
            r = ObjectManipulations.get_item_with_limit(self._internal_token, self._server_pub_key, "bank_account_list/", str(self.limit), '/', resp_data[len(resp_data)-1]['_id'])
            self.assertEqual(r.status_code, 200)
            resp_data = r.json()
            self.assertEqual(ObjectManipulations.id_not_in_array(resp_data, array_ids_doc), True)
            array_ids_doc.extend(ObjectManipulations.get_array_ids_doc(resp_data))
            if (len(resp_data)==1):
                break