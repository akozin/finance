__author__='Andrii Kozin'

import unittest
import requests
from helper import repeat, measure_time
from objectmanipulations import ObjectManipulations

class TestPaymentPlan(unittest.TestCase):
    payload = {'items': {'0': {'amount': '0.00', 'payment_type': '1'}}}
    payload_for_put = dict(item=dict(amount='0.01', payment_type='1'))
    N = 10
    limit = 3

    PAYMENT_TYPE_ELEMENT_NUMBER = 3
    PAYMENT_PLAN_ELEMENT_NUMBER = 3

    @classmethod
    def setUpClass(self):
        self.ids_to_delete = []
        self.ids_to_delete_for_payment_type = []
        (self._facebook_user_id, self._internal_token, self._token_update_time, self._server_pub_key) = ObjectManipulations.generate_user()

    @classmethod
    def tearDownClass(self):
        ObjectManipulations.delete_item(self._internal_token, self._server_pub_key, "payment_plan_list", self.ids_to_delete)
        ObjectManipulations.delete_item(self._internal_token, self._server_pub_key, "payment_type_list", self.ids_to_delete_for_payment_type)
        ObjectManipulations.remove_user(self._internal_token, self._facebook_user_id)

    @measure_time(10)
    @repeat(10)
    def test_create_payment_plan(self):
        r = ObjectManipulations.create_new_item(self._internal_token, self._server_pub_key, self.payload, "payment_plan_list")
        self.assertEqual(r.status_code, 201)
        resp_data = r.json()
        self.ids_to_delete.append(resp_data['0']['_id'])
        self.assertEqual(resp_data['0']['payment_type'], '1')
        self.assertEqual(resp_data['0']['doc_type'], 'payment_plan_doc_type')

    @measure_time()
    def test_delete_payment_plan_after_create_one(self):
        resp_data = ObjectManipulations.create_new_item(self._internal_token, self._server_pub_key, self.payload, "payment_plan_list").json()
        id_to_delete = resp_data['0']['_id']
        r = ObjectManipulations.delete_item(self._internal_token, self._server_pub_key, "payment_plan_list", [id_to_delete])
        self.assertEqual(r.status_code, 200)
        resp_data = r.json()
        self.assertEqual(resp_data['0']['_id'], id_to_delete)

    @measure_time()
    def test_get_payment_plans(self):
        r = ObjectManipulations.get_item(self._internal_token, self._server_pub_key, "payment_plan_list")
        resp_data = r.json()
        if len(resp_data):
            for _item in resp_data:
                self.assertTrue(_item['_id'] in self.ids_to_delete)
        self.assertEqual(r.status_code, 200)

    @measure_time()
    def test_get_payment_plan(self):
        resp_data = ObjectManipulations.create_new_item(self._internal_token, self._server_pub_key, self.payload, "payment_plan_list").json()
        self.ids_to_delete.append(resp_data['0']['_id'])
        id_to_get = resp_data['0']['_id']
        r = ObjectManipulations.get_item(self._internal_token, self._server_pub_key, "payment_plan/", id_to_get)
        self.assertEqual(r.status_code, 200)
        resp_data = r.json()
        self.assertEqual(resp_data['_id'], id_to_get)

    @measure_time()
    def test_put_payment_plan(self):
        resp_data = ObjectManipulations.create_new_item(self._internal_token, self._server_pub_key, self.payload, "payment_plan_list").json()
        id_to_put = resp_data['0']['_id']
        self.ids_to_delete.append(id_to_put)
        r = ObjectManipulations.put_item(self._internal_token, self._server_pub_key, self.payload_for_put, "payment_plan/", id_to_put)
        resp_data = r.json()
        self.assertEqual(r.status_code, 200)
        self.assertEqual(id_to_put,resp_data['_id'])

    @measure_time()
    def test_get_by_payment_type(self):
        i = 0
        _payload_items = {}
        while i < self.PAYMENT_TYPE_ELEMENT_NUMBER:
            _payload_items[str(i)] = dict(name='test1')
            i += 1
        _payload = {'items' : _payload_items}
        resp_data = ObjectManipulations.create_new_item(self._internal_token, self._server_pub_key, _payload, "payment_type_list").json()

        for item_index in resp_data:
            payment_type_item = resp_data[item_index]
            self.ids_to_delete_for_payment_type.append(payment_type_item['_id'])
            i = 0
            _payload_items = {}
            while i < self.PAYMENT_PLAN_ELEMENT_NUMBER:
                _payload_items[str(i)] = dict(amount='0.00', payment_type=payment_type_item['_id'])
                i += 1
            _payload = {'items' : _payload_items}
            resp_data1 = ObjectManipulations.create_new_item(self._internal_token, self._server_pub_key, _payload, "payment_plan_list").json()
            for item in resp_data1:
                payment_plan_item = resp_data1[item]
                self.ids_to_delete.append(payment_plan_item['_id'])
            r = ObjectManipulations.get_item(self._internal_token, self._server_pub_key, "payment_plan_list/get_by_payment_type/", payment_type_item['_id'])
            resp_data2 = r.json()
            self.assertEqual(len(resp_data2), self.PAYMENT_PLAN_ELEMENT_NUMBER)
            self.assertEqual(r.status_code, 200)
            for _item in resp_data2:
                self.assertTrue(_item['payment_type'] == payment_type_item['_id'])

    @measure_time()
    def test_get_payment_plan_with_limit(self):
        i = 0
        _payload_items = {}
        while i < self.N:
            _payload_items[str(i)] = {'amount': '0.00', 'payment_type': '1'}
            i += 1
        _payload = {'items' : _payload_items}
        resp_data = ObjectManipulations.create_new_item(self._internal_token, self._server_pub_key, _payload, "payment_plan_list").json()

        for i in resp_data:
            self.ids_to_delete.append(resp_data[i]['_id'])

        r = ObjectManipulations.get_item_with_limit(self._internal_token, self._server_pub_key, "payment_plan_list/", str(self.limit))
        self.assertEqual(r.status_code, 200)
        resp_data = r.json()
        array_ids_doc = ObjectManipulations.get_array_ids_doc(resp_data)
        while (len(resp_data) <= self.limit):
            r = ObjectManipulations.get_item_with_limit(self._internal_token, self._server_pub_key, "payment_plan_list/", str(self.limit), '/', resp_data[len(resp_data)-1]['_id'])
            self.assertEqual(r.status_code, 200)
            resp_data = r.json()
            self.assertEqual(ObjectManipulations.id_not_in_array(resp_data, array_ids_doc), True)
            array_ids_doc.extend(ObjectManipulations.get_array_ids_doc(resp_data))
            if (len(resp_data)==1):
                break