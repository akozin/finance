__author__='Andrii Kozin'
import time
from file_name_generator import file_time
import csv

def repeat(times):
    def decorator(func):
        def _decorator(self, *args, **kwargs):
            for i in range(times):
                self.setUp()
                func(self, *args, **kwargs)
                self.name_func = func.__name__
                self.tearDown()
            print '\n%s ran %d times' % (func.__name__, times)
        return _decorator

    return decorator

def measure_time(times=1):
    def decorator(func):
        def _decorator(self, *args, **kwargs):
            rec = []
            start = time.time()
            func(self, *args, **kwargs)
            finish = time.time()
            name_test = self.__class__.__name__ + "."
            if func.__name__ == "_decorator":
                name_test += self.name_func
            else:
                name_test += func.__name__
            rec.append(name_test)
            rec.append(str((finish-start)/times))
            with open(file_time.generate_file_name(), 'a+b') as f:
                writer = csv.writer(f, delimiter=",")
                writer.writerow(rec)
        return _decorator
    return decorator