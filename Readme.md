How to start development:
1) download project sources;
2) change dir to downloaded folder;
3) run $ virtualenv env;
4) install packages:
$ env/bin/pip install flask flask-restful Flask-CouchDB Flask-OAuthlib requests rsa

For development purpose we will use facebook app:
app id : 235380669907067
app secret : 2519c47350acb7306a7e057d0573eddd


Modify your /etc/hosts by adding new line in the end

127.0.0.1       m.mirsoft.tk

If you are accessing to the virtual machine you should modify your hosts file on
windows/linux machine.
For windows file stored in c:\windows\system32\hosts

192.168.56.2       m.mirsoft.tk