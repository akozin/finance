from flask.templating import render_template

__author__ = 'z4i'


class BaseView(object):

    def __init__(self, page, **kwargs):
        self._page = page
        self._kwargs = kwargs

    def add_kwarg(self, key, value):
        self._kwargs[key] = value

    def render_page(self):
        return render_template(self._page, **self._kwargs)