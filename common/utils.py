__author__='Andrii Kozin'

class DocumentToJsonConverter(object):
	
	@staticmethod
	def convert(document):
		json = {}
		for key, value in document.items():
			json[key] = value

		return json