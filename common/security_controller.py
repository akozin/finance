__author__='Andrii Kozin'

from flask.globals import g
from app.mod_rest import models
import rsa
import base64
import config

class UserController(object):
    def __init__(self, arg):
        super(UserController, self).__init__()
        self.arg = arg
    
    @staticmethod
    def extract_user_from_token(token):
        # here we expect base64(rsa.encrypt(internal_token|request_time, server_pub_key))
        
        # 1. decode base64
        raw_data = base64.b64decode(token)

        # 2. decrypt using private key
        decrypted_data = rsa.decrypt(raw_data, config.RSA_SERVER_PRIV_KEY)
        _data = decrypted_data.split('|')
        if len(_data) != 2:
            return (None, None)

        _internal_user_token = _data[0]
        _request_time = _data[1]

        return (_internal_user_token, _request_time)

    @staticmethod
    def check_user_credentials(token):
        (_internal_user_token, _request_time) = UserController.extract_user_from_token(token)
        
        if _internal_user_token is None:
            raise Exception("User token is not recognised...")

        _kwargs = dict()
        func_name = models.User.get_by_internal_token
        _kwargs['key'] = _internal_user_token
        _items_to_emit = func_name(g.couch, **_kwargs)

        if len(_items_to_emit.rows) != 1:
            raise Exception("User was not found by token id...")

        _user = _items_to_emit.rows[0]
        if _user is None:
            raise Exception("User is not recognised...")

        return _user
