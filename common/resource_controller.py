__author__='Andrii Kozin'

from flask.globals import g
from flask.ext.restful import Resource
from security_controller import UserController
from utils import DocumentToJsonConverter

class ParametersController(object):
    def __init__(self, *args, **kwargs):
        super(ParametersController, self).__init__(*args, **kwargs)
        
    def get_items(self):
        raise Exception("Not implemented!")

    def get_item(self):
        raise Exception("Not implemented!")

    def get_token(self):
        raise Exception("Not implemented!") 


class ResourceController(Resource, ParametersController):
    def __init__(self, *args, **kwargs):
        super(ResourceController, self).__init__(*args, **kwargs)
        self.resource_type = None

    def get(self, document_id):
        #abort_if_todo_doesnt_exist(todo_id)
        try:
            _user = UserController.check_user_credentials(self.get_token())
        except Exception as e:
            return dict(error=str(e)), 401
        
        _kwargs = dict()
        func_name = getattr(self.resource_type, 'get_all_items')
        _kwargs['key'] = [1, _user.id, document_id]
        _items_to_emit = func_name(g.couch, **_kwargs)

        if len(_items_to_emit.rows) != 1:
            return "", 500

        document = _items_to_emit.rows[0]
        json = DocumentToJsonConverter.convert(document)
        return json, 200

    def delete(self, document_id):
        #abort_if_todo_doesnt_exist(todo_id)
        try:
            _user = UserController.check_user_credentials(self.get_token())
        except Exception as e:
            return dict(error=str(e)), 401

        _kwargs = dict()
        func_name = getattr(self.resource_type, 'get_all_items')
        _kwargs['key'] = [1, _user.id, document_id]
        _items_to_emit = func_name(g.couch, **_kwargs)

        if len(_items_to_emit.rows) != 1:
            return "", 500

        document = _items_to_emit.rows[0]
        document._data['_deleted'] = True
        document.store(g.couch)
        return {'_id' : document_id}, 200

    def put(self, document_id):
        try:
            _user = UserController.check_user_credentials(self.get_token())
        except Exception as e:
            return dict(error=str(e)), 401

        _kwargs = dict()
        func_name = getattr(self.resource_type, 'get_all_items')
        _kwargs['key'] = [1, _user.id, document_id]
        _items_to_emit = func_name(g.couch, **_kwargs)

        if len(_items_to_emit.rows) != 1:
            return "", 500

        document = _items_to_emit.rows[0]
        _args = self.get_item()

        for key in _args:
            if key in document._data.keys():
                document._data[key] = _args[key]
        document = document.store(g.couch)
        _json = DocumentToJsonConverter.convert(document)
        return _json, 200
      

class ListResourceController(Resource, ParametersController):
    def __init__(self, *args, **kwargs):
        super(ListResourceController, self).__init__(*args, **kwargs)
        self.resource_type = None

    def get(self, limit=None, startkey_docid=None, req_name=None, key=None):
        try:
            _user = UserController.check_user_credentials(self.get_token())
        except Exception as e:
            return dict(error=str(e)), 401

        return_objects = []
        _kwargs = dict()

        if limit is not None:
            _kwargs['limit'] = limit
        
        if req_name is not None:
            if req_name in dir(self.resource_type):
                func_name = getattr(self.resource_type, req_name)
                if key is not None:
                    _kwargs['key'] = [1, _user.id, key]
        else:
            func_name = getattr(self.resource_type, 'get_all_items')
            _kwargs['key'] = [2, _user.id]
        

        if startkey_docid is not None:
            _kwargs['start_key'] = [1, _user.id, startkey_docid]
            _kwargs['end_key'] = [1, _user.id, '99999999999999999999999999999999']
            del _kwargs['key']

        _items_to_emit = func_name(g.couch, **_kwargs)

        for _row in _items_to_emit.rows:
            document = _row
            if document.user_id == _user.id:
                json = DocumentToJsonConverter.convert(document)
                return_objects.append(json)
        return return_objects, 200

    def post(self):
        try:
            _user = UserController.check_user_credentials(self.get_token())
        except Exception as e:
            return dict(error=str(e)), 401

        documents = self.get_items()
        created_objects = {}

        for index_item in documents:
            doc_to_save = self.resource_type()
            document = documents[index_item]
            for key in document:
                doc_to_save._data[key] = document[key]

            doc_to_save.user_id = _user.id
            doc_to_save = doc_to_save.store(g.couch)
            json = DocumentToJsonConverter.convert(doc_to_save)
            created_objects[index_item] = json

        return created_objects, 201

    def find_key_by_value(self, dictionary, value_to_search):
        for key, value in dictionary.iteritems():
            if value == value_to_search:
                return key
        return None

    def delete(self):
        try:
            _user = UserController.check_user_credentials(self.get_token())
        except Exception as e:
            return dict(error=str(e)), 401

        _func_name = getattr(self.resource_type, 'get_all_items')
        
        documents = self.get_items()
        deleted_objects = {}
        _keys = []
        for index_item in documents:
            _document_id = documents[index_item]
            _keys.append([1, _user.id, _document_id])

        _kwargs = {}
        _kwargs['keys'] = _keys

        _items_to_emit = _func_name(g.couch, **_kwargs)
        
        for _row in _items_to_emit.rows:
            _row._data['_deleted'] = True
            _row.store(g.couch)
            deleted_objects[self.find_key_by_value(documents, _row.id)] = {'_id' : _row.id}

        return deleted_objects, 200
