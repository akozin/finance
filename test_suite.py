__author__ = 'Andrii Kozin'

from unittest import TestLoader, TextTestRunner, TestSuite, defaultTestLoader
from tests.testcases.fb_user import FbUser

#TODO: add here new modules which should be ran
testmodules = [
    'tests.testcases.test_bank_account',
    'tests.testcases.test_currency',
    'tests.testcases.daily_status',
    'tests.testcases.income',
    'tests.testcases.income_plan',
    'tests.testcases.income_type',
    'tests.testcases.payment',
    'tests.testcases.payment_plan',
    'tests.testcases.payment_type',
]

def load_test_cases_by_module_names(test_modules):
    suite = TestSuite()

    for t in test_modules:
        try:
            # If the module defines a suite() function, call it to get the suite.
            mod = __import__(t, globals(), locals(), ['suite'])
            suitefn = getattr(mod, 'suite')
            suite.addTest(suitefn())
        except (ImportError, AttributeError):
            # else, just load all the test cases from the module.
            suite.addTest(defaultTestLoader.loadTestsFromName(t))

    return suite

if __name__ == '__main__':
    loader = TestLoader()
    suite = load_test_cases_by_module_names(testmodules)

    FbUser.get_facebook_user(len(testmodules))
    runner = TextTestRunner(verbosity=2)
    runner.run(suite)
